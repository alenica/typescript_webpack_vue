import Vue from 'vue'
import Component from 'vue-class-component'
import {Prop as prop } from 'vue-property-decorator'

@Component
export default class MainComponent extends Vue {
    @prop()
  private message: string | undefined;
  constructor() {
    super()
    console.log(this.message)
  }

}
